﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace FriendsApp.Model
{
    public class Friend
    {
        public Friend()
        {
            Notes = new Collection<Note>();
        }

        public int Id { get; set; }

        [Required]
        [MaxLength(15)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }

        [Phone]
        [MaxLength(30)]
        public string PhoneNumber { get; set; }

        [MaxLength(30)]
        [EmailAddress]
        public string Email { get; set; }
        
        public DateTime Birthday { get; set; }

        //property to validate version if there are changes made by other user
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public ICollection<Note> Notes { get; set; }
    }
}
