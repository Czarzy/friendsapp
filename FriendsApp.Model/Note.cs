﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace FriendsApp.Model
{
    public class Note
    {
        public Note()
        {
            Friends = new Collection<Friend>();

        }

        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        public string Title { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public ICollection<Friend> Friends { get; set; } 
    }
}
