﻿//class to make the "menu" load only Id and Name of the friend in friends list.
namespace FriendsApp.Model
{
    public class LookupItem
    {
        public int Id { get; set; }
        public string DisplayItem { get; set; }
    }
}
