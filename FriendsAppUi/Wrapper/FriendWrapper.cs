﻿using System;
using System.Collections.Generic;
using FriendsApp.Model;

//this class wraps the Friend class so we can use INotifyDataErrorInfo here
namespace FriendsAppUi.Wrapper
{
    public class FriendWrapper: ModelWrapper<Friend>
    {
        public FriendWrapper(Friend model) : base(model)
        {

        }

        public int Id { get { return Model.Id; } }

        public string FirstName
        {
            get { return GetValue<string>(); }
            set
            { SetValue(value); }
        }

        public string LastName
        {
            get { return GetValue<string>(); }
            set
            { SetValue(value); }
        }

        public string PhoneNumber
        {
            get { return GetValue<string>(); }
            set
            { SetValue(value); }
        }

        public string Email
        {
            get { return GetValue<string>(); }
            set
            { SetValue(value); }
        }

        public DateTime Birthday
        {
            get { return GetValue<DateTime>(); }
            set
            { SetValue(value); }
        }

        //override Validation from ModelWrapper
        protected override IEnumerable<string> ValidateProperty(string propertyName)
        {
            switch (propertyName)
            {
                case nameof(FirstName):
                    if (FirstName.Length < 3)
                    {
                        yield return "Name must be at least 3 chars long.";
                    }
                    break;
            }
        }
    }
}
