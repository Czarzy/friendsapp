﻿using FriendsApp.Model;
using System;

namespace FriendsAppUi.Wrapper
{
    public class NoteWrapper: ModelWrapper<Note>
    {
        public NoteWrapper(Note model) : base(model)
        {

        }

        //returns Id of the model passed in constructor
        public int Id { get { return Model.Id; } }

        public string Title
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        public string Message
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        public DateTime Date
        {
            get { return GetValue<DateTime>(); }
            set { SetValue(value); }
        }
    }
}
