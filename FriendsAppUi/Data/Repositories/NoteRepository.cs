﻿using FriendsApp.DataAccess;
using FriendsApp.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Repositories
{
    public class NoteRepository : GenericRepository<Note, FriendsAppDbContext>, INoteRepository
    {
        public NoteRepository(FriendsAppDbContext context): base(context)
        {

        }
        //get note by id
        public async override Task<Note> GetByIdAsync(int Id)
        {
          return await _context.Notes
                .Include(n => n.Friends)
                .SingleAsync(n => n.Id == Id);
        }

        public async Task<List<Friend>> GetAllFriendsAsync()
        {
            return await _context.Set<Friend>()
                .ToListAsync();
        }

        public async Task ReloadFriendAsync(int friendId)
        {
            var dbEntityEntry = _context.ChangeTracker.Entries<Friend>()
                .SingleOrDefault(db => db.Entity.Id == friendId);

            if(dbEntityEntry != null)
            {
                await dbEntityEntry.ReloadAsync();
            }
        }
    }
}
