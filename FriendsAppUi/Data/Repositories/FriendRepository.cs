﻿using FriendsApp.DataAccess;
using FriendsApp.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Repositories
{
    public class FriendRepository : GenericRepository<Friend, FriendsAppDbContext>,
                                    IFriendRepository
    {
        public FriendRepository(FriendsAppDbContext context)
            : base(context)
        {
        }

        //get friend by id
        public override async Task<Friend> GetByIdAsync(int friendId)
        {
            return await _context.Friends.SingleAsync(f => f.Id == friendId);

        }

        //is friend attached to any note?
        public async Task<bool> HasNotes(int friendId)
        {
            return await _context.Notes.AsNoTracking()
                .Include(m => m.Friends)
                .AnyAsync(m => m.Friends.Any(f => f.Id == friendId));
        }
    }
}
