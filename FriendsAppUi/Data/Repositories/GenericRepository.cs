﻿using System.Data.Entity;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Repositories
{
    public class GenericRepository<TEntity, TContext> : IGenericRepository<TEntity>
        where TEntity: class
        where TContext: DbContext
    {
        public TContext _context;

        public GenericRepository(TContext context)
        {
            _context = context;
        }

        public void Add(TEntity model)
        {
            _context.Set<TEntity>().Add(model);
        }

        public virtual async Task<TEntity> GetByIdAsync(int Id)
        {
            return await _context.Set<TEntity>().FindAsync(Id);
        }

        public bool HasChanges()
        {
            return _context.ChangeTracker.HasChanges();
        }

        public void Remove(TEntity model)
        {
            _context.Set<TEntity>().Remove(model);
        }

        public async Task SaveAsync()
        {
           await _context.SaveChangesAsync();
        }
    }
}
