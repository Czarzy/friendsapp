﻿using System.Threading.Tasks;

namespace FriendsAppUi.Data.Repositories
{
    public interface IGenericRepository<T>
    {
        Task<T> GetByIdAsync(int Id);
        Task SaveAsync();
        bool HasChanges();
        void Add(T friend);
        void Remove(T model);
    }
}