﻿using FriendsApp.Model;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Repositories
{
    public interface IFriendRepository: IGenericRepository<Friend>
    {
        Task<bool> HasNotes(int friendId);
    }
}