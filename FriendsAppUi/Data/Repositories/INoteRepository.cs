﻿using FriendsApp.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Repositories
{
    public interface INoteRepository: IGenericRepository<Note>
    {
        Task<List<Friend>> GetAllFriendsAsync();
        Task ReloadFriendAsync(int friendId);
    }
}