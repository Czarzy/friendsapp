﻿using FriendsApp.DataAccess;
using FriendsApp.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Lookups
{
    public class LookupDataService : ILookupDataService, INoteLookupDataService
    {
        private Func<FriendsAppDbContext> _contextCreator;

        public LookupDataService(Func<FriendsAppDbContext> contextCreator)
        {
            _contextCreator = contextCreator;
        }
        //get the ID, FirstNames and LastNames of all friends to the Navigation Panel
        public async Task<IEnumerable<LookupItem>> GetFriendLookupAsync()
        {
            using (var ctx = _contextCreator())
            {
                return await ctx.Friends.AsNoTracking()
                    .Select(f =>
                    new LookupItem
                    {
                        Id = f.Id,
                        DisplayItem = f.FirstName + " " + f.LastName
                    })
                    .ToListAsync();
            }
        }
        //get the ID and Title of note to the Navigation Panel
        public async Task<List<LookupItem>> GetNoteLookupAsync()
        {
            using (var ctx = _contextCreator())
            {
                return await ctx.Notes.AsNoTracking()
                    .Select(n =>
                    new LookupItem
                    {
                        Id = n.Id,
                        DisplayItem = n.Title
                    })
                    .ToListAsync();
            }
        }
    }
}
