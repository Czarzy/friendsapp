﻿using FriendsApp.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FriendsAppUi.Data.Lookups
{
    public interface INoteLookupDataService
    {
        Task<List<LookupItem>> GetNoteLookupAsync();
    }
}
