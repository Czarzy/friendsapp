﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FriendsApp.Model;

namespace FriendsAppUi.Data.Lookups
{
    public interface ILookupDataService
    {
        Task<IEnumerable<LookupItem>> GetFriendLookupAsync();
    }
}