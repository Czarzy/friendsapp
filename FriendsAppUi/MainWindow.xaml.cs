﻿using FriendsAppUi.ViewModel;
using System.Windows;
using System;
using MahApps.Metro.Controls;
using System.ComponentModel;

namespace FriendsAppUi
{
    public partial class MainWindow : MetroWindow
    {
        private MainViewModel _viewModel;

        public MainWindow(MainViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel;
            DataContext = _viewModel;
            Loaded += MainWindow_Loaded;
            Closing += OnClosing;
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
           await _viewModel.LoadAsync();
        }

        //on application shut down
        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            if (MessageBox.Show(this, "Do you really want to close this application?", "Exit", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            {
                cancelEventArgs.Cancel = true;
            }
        }

        private void about_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("FriendsApp 1.0\n Made by Rafal Czarzasty.", "About");
        }
    }
}
