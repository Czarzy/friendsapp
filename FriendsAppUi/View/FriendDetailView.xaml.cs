﻿
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace FriendsAppUi.View
{
    /// <summary>
    /// Interaction logic for FriendDetailView.xaml
    /// </summary>
    public partial class FriendDetailView : UserControl
    {
        public FriendDetailView()
        {
            InitializeComponent();
        }

        private void TextBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            
        }

        //set to allow only numbers
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9, -]");
            return !regex.IsMatch(text);
        }
    }
}
