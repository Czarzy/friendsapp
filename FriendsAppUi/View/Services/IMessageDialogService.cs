﻿using System.Threading.Tasks;

namespace FriendsAppUi.View.Services
{
    public interface IMessageDialogService
    {
        Task<MessageDialogResult> ShowOkCancelDialogAsync(string text, string title);

        Task ShowInfoDialogAsync(string text);
    }
}