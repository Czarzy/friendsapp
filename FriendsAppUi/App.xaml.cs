﻿using Autofac;
using FriendsAppUi.Startup;
using FriendsAppUi.View.Services;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;

namespace FriendsAppUi
{
    public partial class App : Application
    {

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var bootstrapper = new Bootstrapper();
            var container = bootstrapper.Bootstrap();

            var mainWindow = container.Resolve<MainWindow>();
            mainWindow.Show();
        }

        private void Application_DispatcherUnhandledException(object sender, 
            System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Unknown Error has occured. Please contact the producer." + Environment.NewLine + e.Exception.Message, "Unknown Error");
            e.Handled = true;
        }
    }
}
