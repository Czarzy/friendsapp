﻿using System.Threading.Tasks;

namespace FriendsAppUi.ViewModel
{
    public interface INavigationViewModel
    {
        Task LoadAsync();
    }
}