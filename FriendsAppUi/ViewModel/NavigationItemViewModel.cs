﻿using FriendsAppUi.Event;
using Prism.Commands;
using Prism.Events;
using System.Windows.Input;

namespace FriendsAppUi.ViewModel
{
    public class NavigationItemViewModel: ViewModelBase
    {
        private string _displayItem;
        private string _detailViewModelName;

        private IEventAggregator _eventAggregator;

        public NavigationItemViewModel(int id, string displayItem, IEventAggregator eventAggregator, string detailViewModelName)
        {
            Id = id;
            DisplayItem = displayItem;
            _detailViewModelName = detailViewModelName;
            _eventAggregator = eventAggregator;
            OpenDetailViewCommand = new DelegateCommand(OnOpenDetailViewExecute);
        }

        public int Id { get; }

        public string DisplayItem
        {
            get { return _displayItem; }
            set {
                _displayItem = value;
                OnPropertyChanged();
            }
        }

        public ICommand OpenDetailViewCommand { get; }

        //Open detail view in navigation panel
        private void OnOpenDetailViewExecute()
        {
            _eventAggregator.GetEvent<OpenDetailViewEvent>()
                .Publish(
                new OpenDetailViewEventArgs
                {
                    Id = Id,
                    ViewModelName = _detailViewModelName
                });
        }

    }
}
