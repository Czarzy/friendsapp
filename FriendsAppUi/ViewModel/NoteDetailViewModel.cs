﻿using FriendsApp.Model;
using FriendsAppUi.Data.Repositories;
using FriendsAppUi.View.Services;
using FriendsAppUi.Wrapper;
using Prism.Commands;
using Prism.Events;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using FriendsAppUi.Event;

namespace FriendsAppUi.ViewModel
{
    public class NoteDetailViewModel: DetailViewModelBase, INoteDetailViewModel
    {
        private INoteRepository _noteRepository;
        private NoteWrapper _note;
        private Friend _selectedAvailableFriend;
        private Friend _selectedAddedFriend;
        private List<Friend> _allFriends;

        public NoteDetailViewModel(IEventAggregator eventAggregator, IMessageDialogService messageDialogService, INoteRepository noteRepository)
            :base(eventAggregator, messageDialogService)
        {
            _noteRepository = noteRepository;
            eventAggregator.GetEvent<AfterDetailSavedEvent>().Subscribe(AfterDetailSaved);
            eventAggregator.GetEvent<AfterDetailDeletedEvent>().Subscribe(AfterDetailDeleted);

            AddedFriends = new ObservableCollection<Friend>();
            FriendsDatabase = new ObservableCollection<Friend>();
            AddFriendCommand = new DelegateCommand(OnAddFriendExecute, OnAddFriendCanExecute);
            RemoveFriendCommand = new DelegateCommand(OnRemoveFriendExecute, OnRemoveFriendCanExecute);

        }

        public NoteWrapper Note
        {
            get { return _note; }
            private set
            {
                _note = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddFriendCommand { get; }

        public ICommand RemoveFriendCommand { get; }

        public ObservableCollection<Friend> AddedFriends { get; }

        public ObservableCollection<Friend> FriendsDatabase { get; }


        public Friend SelectedAvailableFriend
        {
            get { return _selectedAvailableFriend; }
            set {
                _selectedAvailableFriend = value;
                OnPropertyChanged();
                ((DelegateCommand)AddFriendCommand).RaiseCanExecuteChanged();
            }
        }

        public Friend SelectedAddedFriend
        {
            get { return _selectedAddedFriend; }
            set
            {
                _selectedAddedFriend = value;
                OnPropertyChanged();
                ((DelegateCommand)RemoveFriendCommand).RaiseCanExecuteChanged();
            }
        }

        public override async Task LoadAsync(int noteId)
        {
            var note = noteId > 0
                ? await _noteRepository.GetByIdAsync(noteId)
                : CreateNewNote();

            Id = noteId;

            InitializeNote(note);

            _allFriends = await _noteRepository.GetAllFriendsAsync();

            SetupPicklist();
        }

        protected override async void OnDeleteExecute()
        {
            var result = await MessageDialogService.ShowOkCancelDialogAsync($"Do you really want to delete note named {Note.Title}?", "Delete Note");
            if (result == MessageDialogResult.Ok)
            {
                _noteRepository.Remove(Note.Model);
                await _noteRepository.SaveAsync();
                RaiseDetailDeletedEvent(Note.Id);
            }
        }

        protected override bool OnSaveCanExecute()
        {
            return Note != null && !Note.HasErrors && HasChanges;
        }

        protected override async void OnSaveExecute()
        {
            await _noteRepository.SaveAsync();
            HasChanges = _noteRepository.HasChanges();
            Id = Note.Id;
            RaiseDetailSavedEvent(Note.Id, Note.Title);
        }

        private Note CreateNewNote()
        {
            var note = new Note()
            {
                Date = DateTime.Now.Date
            };
            _noteRepository.Add(note);
            return note;
        }

        private void InitializeNote(Note note)
        {
            Note = new NoteWrapper(note);
            Note.PropertyChanged += (s, e) =>
            {
                if (!HasChanges)
                {
                    HasChanges = _noteRepository.HasChanges();
                }

                if (e.PropertyName == nameof(Note.HasErrors))
                {
                    ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
                }
                if(e.PropertyName == nameof(note.Title))
                {
                    SetTitle();
                }
            };
            ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();

            //trigger validation
            if (Note.Id == 0)
            {
                Note.Title = "";
            }
            SetTitle();
        }

        private void SetTitle()
        {
            Title = Note.Title;
        }

        private void SetupPicklist()
        {
            var noteFriendsId = Note.Model.Friends.Select(f => f.Id).ToList();
            var addedFriends = _allFriends.Where(f => noteFriendsId.Contains(f.Id)).OrderBy(f => f.FirstName);
            var availableFriends = _allFriends.Except(addedFriends).OrderBy(f => f.FirstName);

            AddedFriends.Clear();
            FriendsDatabase.Clear();
            foreach(var addedFriend in addedFriends)
            {
                AddedFriends.Add(addedFriend);
            }
            foreach(var availableFriend in availableFriends)
            {
                FriendsDatabase.Add(availableFriend);
            }

        }

        private void OnRemoveFriendExecute()
        {
            var friendToRemove = SelectedAddedFriend;

            Note.Model.Friends.Remove(friendToRemove);
            AddedFriends.Remove(friendToRemove);
            FriendsDatabase.Add(friendToRemove);
            HasChanges = _noteRepository.HasChanges();
            ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
        }

        private bool OnRemoveFriendCanExecute()
        {
            return SelectedAddedFriend != null;
        }

        private bool OnAddFriendCanExecute()
        {
            return SelectedAvailableFriend != null;
        }

        private void OnAddFriendExecute()
        {
            var friendToAdd = SelectedAvailableFriend;

            Note.Model.Friends.Add(friendToAdd);
            AddedFriends.Add(friendToAdd);
            FriendsDatabase.Remove(friendToAdd);
            HasChanges = _noteRepository.HasChanges();
            ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
        }

        private async void AfterDetailSaved(AfterDetailSavedEventArgs args)
        {
            //if new data are saved, refresh opened detail view model
            if(args.ViewModelName == nameof(FriendDetailViewModel))
            {
                await _noteRepository.ReloadFriendAsync(args.Id);
                _allFriends = await _noteRepository.GetAllFriendsAsync();
                SetupPicklist();
            }
        }

        private async void AfterDetailDeleted(AfterDetailDeletedEventArgs args)
        {
            //if new data are saved, refresh opened detail view model
            if (args.ViewModelName == nameof(FriendDetailViewModel))
            {
                _allFriends = await _noteRepository.GetAllFriendsAsync();
                SetupPicklist();
            }
        }
    }
}
