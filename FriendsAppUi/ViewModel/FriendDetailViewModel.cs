﻿using FriendsApp.Model;
using FriendsAppUi.Data.Repositories;
using FriendsAppUi.View.Services;
using FriendsAppUi.Wrapper;
using Prism.Commands;
using Prism.Events;
using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsAppUi.ViewModel
{
    public class FriendDetailViewModel : DetailViewModelBase, IFriendDetailViewModel
    {
        private IFriendRepository _friendRepository;
        private FriendWrapper _friend;

        public FriendDetailViewModel(IFriendRepository friendRepository, IEventAggregator eventAggregator, IMessageDialogService messageDialogService)
            :base(eventAggregator, messageDialogService)
        {
            _friendRepository = friendRepository;
        }

        //Load detailed Friend data by Id to detail view
        public override async Task LoadAsync(int friendId)
        {
            var friend = friendId > 0
                ? await _friendRepository.GetByIdAsync(friendId)
                : CreateNewFriend();

            Id = friendId;

            InitializeFriend(friend);   
        }

        private void InitializeFriend(Friend friend)
        {
            Friend = new FriendWrapper(friend);
            //if model has errors, change state of the save button to be disabled, else - enable it
            Friend.PropertyChanged += (s, e) =>
            {
                if (!HasChanges)
                {
                    HasChanges = _friendRepository.HasChanges();
                }
                if (e.PropertyName == nameof(Friend.HasErrors))
                {
                    ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();
                }
                if (e.PropertyName == nameof(Friend.FirstName) || e.PropertyName == nameof(Friend.LastName))
                {
                    SetTitle();
                }
            };
                ((DelegateCommand)SaveCommand).RaiseCanExecuteChanged();

                //Trigger validation on new user creation
                if (Friend.Id == 0)
                {
                    Friend.FirstName = "";
                    Friend.LastName = "";
                    Friend.Birthday = DateTime.Now;
                }
                SetTitle();
        }

        private void SetTitle()
        {
            Title = $"{Friend.FirstName} {Friend.LastName}";
        }

        public FriendWrapper Friend
        {
            get { return _friend; }
            private set
            {
                _friend = value;
                OnPropertyChanged();
            }
        }

        protected override async void OnSaveExecute()
        {
            await SaveOnOptimisticConcurrencyAsync(_friendRepository.SaveAsync, () => 
            {
                HasChanges = _friendRepository.HasChanges();
                Id = Friend.Id;
                RaiseDetailSavedEvent(Friend.Id, Friend.FirstName + " " + Friend.LastName);
            });
            try
            {
                await _friendRepository.SaveAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                var databaseValues = ex.Entries.Single().GetDatabaseValues();

                if (databaseValues == null)
                {
                    await MessageDialogService.ShowInfoDialogAsync("The entity has been deleted by another user.");
                    RaiseDetailDeletedEvent(Id);
                    return;
                }
                var result = await MessageDialogService.ShowOkCancelDialogAsync("The entity was changed in the meantime by other user. " +
                    "Click Ok to sava your changes anyway, or Cancel to reload entity from Database.", "Error");

                if(result == MessageDialogResult.Ok)
                {
                    var entry = ex.Entries.Single();
                    entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    await _friendRepository.SaveAsync();
                }
                else
                {
                    await ex.Entries.Single().ReloadAsync();
                    await LoadAsync(Friend.Id);
                }
            }
        }

        protected override async void OnDeleteExecute()
        {
            if(await _friendRepository.HasNotes(Friend.Id))
            {
                await MessageDialogService.ShowInfoDialogAsync($"{Friend.FirstName} {Friend.LastName} can't be deleted - he's attached to at least one note.");
                return;
            }

            var result = await MessageDialogService.ShowOkCancelDialogAsync("Delete this Friend?", "Delete Friend");
            if (result == MessageDialogResult.Ok)
            {
                _friendRepository.Remove(Friend.Model);
                await _friendRepository.SaveAsync();
                RaiseDetailDeletedEvent(Friend.Id);
            }
        }

        //can be SaveCommand executed?
        protected override bool OnSaveCanExecute()
        {
            return Friend != null && !Friend.HasErrors && HasChanges;
        }

        //create new friend method
        private Friend CreateNewFriend()
        {
            var friend = new Friend();
            _friendRepository.Add(friend);
            return friend;
        }
    }
}
