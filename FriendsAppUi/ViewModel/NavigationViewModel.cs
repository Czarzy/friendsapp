﻿using FriendsAppUi.Data.Lookups;
using FriendsAppUi.Event;
using Prism.Events;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace FriendsAppUi.ViewModel
{
    public class NavigationViewModel : ViewModelBase, INavigationViewModel
    {
        private ILookupDataService _friendLookupDataService;
        private IEventAggregator _eventAggregator;
        private INoteLookupDataService _noteLookupDataService;

        //get collection of friends data for navigation panel
        public NavigationViewModel(ILookupDataService lookupDataService, INoteLookupDataService noteLookupDataService,
            IEventAggregator eventAggregator)
        {
            _friendLookupDataService = lookupDataService;
            _eventAggregator = eventAggregator;
            _noteLookupDataService = noteLookupDataService;
            Friends = new ObservableCollection<NavigationItemViewModel>();
            Notes = new ObservableCollection<NavigationItemViewModel>();
            _eventAggregator.GetEvent<AfterDetailSavedEvent>().Subscribe(AfterDetailSaved);
            _eventAggregator.GetEvent<AfterDetailDeletedEvent>().Subscribe(AfterDetailDeleted);
        }      

        public async Task LoadAsync()
        {
            var lookup = await _friendLookupDataService.GetFriendLookupAsync();
            Friends.Clear();
            foreach(var item in lookup)
            {
                Friends.Add(new NavigationItemViewModel(item.Id, item.DisplayItem, _eventAggregator, nameof(FriendDetailViewModel)));
            }
            lookup = await _noteLookupDataService.GetNoteLookupAsync();
            Notes.Clear();
            foreach (var item in lookup)
            {
                Notes.Add(new NavigationItemViewModel(item.Id, item.DisplayItem, _eventAggregator, nameof(NoteDetailViewModel)));
            }
        }

        public ObservableCollection<NavigationItemViewModel> Friends { get; }

        public ObservableCollection<NavigationItemViewModel> Notes { get; }

        private void AfterDetailDeleted(AfterDetailDeletedEventArgs args)
        {
            switch(args.ViewModelName)
            {
                case (nameof(FriendDetailViewModel)):
                    AfterDetailDeleted(Friends, args);
                    break;
                case (nameof(NoteDetailViewModel)):
                    AfterDetailDeleted(Notes, args);
                    break;
            }
        }

        private void AfterDetailDeleted(ObservableCollection<NavigationItemViewModel> items, AfterDetailDeletedEventArgs args)
        {
            var item = items.SingleOrDefault(i => i.Id == args.Id);
            if (item != null)
            {
                items.Remove(item);
            }
        }

        //after save button clicked update or create new entry at navigate panel
        private void AfterDetailSaved(AfterDetailSavedEventArgs args)
        {
            switch(args.ViewModelName)
            {
                case (nameof(FriendDetailViewModel)):
                AfterDetailSaved(Friends, args);
                    break;
                case (nameof(NoteDetailViewModel)):
                    AfterDetailSaved(Notes, args);
                    break;
            }
        }

        private void AfterDetailSaved(ObservableCollection<NavigationItemViewModel> items, AfterDetailSavedEventArgs args)
        {
            var lookupItem = items.SingleOrDefault(i => i.Id == args.Id);
            if (lookupItem == null)
            {
                items.Add(new NavigationItemViewModel(args.Id, args.DisplayMember, _eventAggregator, args.ViewModelName));
            }
            else
            {
                lookupItem.DisplayItem = args.DisplayMember;
            }
        }
    }
}
