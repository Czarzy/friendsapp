﻿using System.Threading.Tasks;

namespace FriendsAppUi.ViewModel
{
    public interface IDetailViewModel
    {
        Task LoadAsync(int id);
        bool HasChanges { get; }
        int Id { get; }
    }
}