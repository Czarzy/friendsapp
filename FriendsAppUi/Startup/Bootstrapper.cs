﻿using Autofac;
using FriendsApp.DataAccess;
using FriendsAppUi.Data.Lookups;
using FriendsAppUi.Data.Repositories;
using FriendsAppUi.View.Services;
using FriendsAppUi.ViewModel;
using Prism.Events;
//it's a class using Autofac to auto-create instances
namespace FriendsAppUi.Startup
{
    public class Bootstrapper
    {
        public IContainer Bootstrap()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();

            builder.RegisterType<FriendsAppDbContext>().AsSelf();

            builder.RegisterType<MainWindow>().AsSelf();

            builder.RegisterType<NoteRepository>().As<INoteRepository>();
            builder.RegisterType<MessageDialogService>().As<IMessageDialogService>();
            builder.RegisterType<MainViewModel>().AsSelf();
            builder.RegisterType<NavigationViewModel>().As<INavigationViewModel>();
            builder.RegisterType<FriendDetailViewModel>()
               .Keyed<IDetailViewModel>(nameof(FriendDetailViewModel));
            builder.RegisterType<NoteDetailViewModel>()
                .Keyed<IDetailViewModel>(nameof(NoteDetailViewModel));
            builder.RegisterType<LookupDataService>().AsImplementedInterfaces();
            builder.RegisterType<FriendRepository>().As<IFriendRepository>();

            return builder.Build();
        }
    }
}
