﻿using FriendsApp.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace FriendsApp.DataAccess
{
    public class FriendsAppDbContext : DbContext
    {
        public FriendsAppDbContext() : base("FriendsAppDb")
        {

        }

        public DbSet<Friend> Friends { get; set; }

        public DbSet<Note> Notes { get; set; }

        //override Pluralizing convention so names of the tables won't be plural
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}