namespace FriendsApp.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friend",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 15),
                        LastName = c.String(nullable: false, maxLength: 30),
                        PhoneNumber = c.String(maxLength: 30),
                        Email = c.String(maxLength: 30),
                        Birthday = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Note",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 30),
                        Message = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NoteFriend",
                c => new
                    {
                        Note_Id = c.Int(nullable: false),
                        Friend_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Note_Id, t.Friend_Id })
                .ForeignKey("dbo.Note", t => t.Note_Id, cascadeDelete: true)
                .ForeignKey("dbo.Friend", t => t.Friend_Id, cascadeDelete: true)
                .Index(t => t.Note_Id)
                .Index(t => t.Friend_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NoteFriend", "Friend_Id", "dbo.Friend");
            DropForeignKey("dbo.NoteFriend", "Note_Id", "dbo.Note");
            DropIndex("dbo.NoteFriend", new[] { "Friend_Id" });
            DropIndex("dbo.NoteFriend", new[] { "Note_Id" });
            DropTable("dbo.NoteFriend");
            DropTable("dbo.Note");
            DropTable("dbo.Friend");
        }
    }
}
