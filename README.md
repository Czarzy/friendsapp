# README #

Welcome to FriendsApp. It's simple friends organizer, where you can add friends data and manage notes including your friends (e.g meeting).

Application was made using WPF, code in c#. It also uses: Prism, Autofac, Entity Framework (it seeds to online database hosted on AppHarbor) and MahApps for the looks.

Link to application download: https://www.dropbox.com/s/iie86y50i4ipen5/FriendsApp.rar?dl=0l
Just run FriendsAppUi application.